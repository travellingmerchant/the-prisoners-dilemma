extends RichTextLabel

#variable that can be changed in the inspector, chooses what textfile to load.
export var text_file_name=""
var type_stack=[]
var is_comment_block=false
var current_content=""

var body_letter_reached=false

var input_title=""
var input_body=""
var input_colour=""
var input_display_title=""

var article_list=[]

class Article:
	var title=""
	var display_title=""
	var colour=""
	var body=""


func _ready():
	if(Globals.current_scene!=""):
		text_file_name=Globals.current_scene
	parse_article()
	show_articles()

func show_articles():
	for i in range(article_list.size()):
		var article_button= Button.new()
		article_button.align=0
		article_button.text=article_list[i].display_title
		article_button.set_name(article_list[i].title)
		article_button.connect("pressed",self,"open_article",[article_list[i]])
		$ScrollContainer.get_node("VBoxContainer").add_child(article_button)

func open_article(var article):
	get_parent().get_node("Back").visible=false
	get_parent().get_node("Article Back").visible=true
	$ScrollContainer.visible=false
	if article.colour=="":
		bbcode_text="[center][color=#ff336b]"+article.title+"[/color][/center]"
	else:
		if article.colour[0]=='#':
			bbcode_text="[center][color="+article.colour+"]"+article.title+"[/color][/center]"
		else:
			bbcode_text="[center][color=#"+article.colour+"]"+article.title+"[/color][/center]"
	bbcode_text+="\n"
	bbcode_text+="\n"
	bbcode_text+=article.body
	for children in $ScrollContainer.get_node("VBoxContainer").get_children():
		children.queue_free()

func parse_article():
	var textfile= File.new()
	var text_file_data=""
	var filepath="res://Textfiles/ArticleTextfiles/"+text_file_name+".txt"
	#open text file to read
	if textfile.file_exists(filepath):
		textfile.open(filepath,textfile.READ)
		text_file_data= textfile.get_as_text()
		textfile.close()
	else:
		print("textFile not found.")
		return
		
	type_stack.push_back("scene");
	for character in text_file_data:
		if character =='$':
			is_comment_block=!is_comment_block
			continue
		if !is_comment_block:
			match character:
				'{':
					type_stack.push_back(current_content)
					current_content=""
				'}':
					match type_stack.back():
						"title":
							input_title=current_content
						"displayTitle":
							input_display_title=current_content
						"body":
							input_body=current_content
						"colour":
							input_colour=current_content
						"article":
							var input_article=Article.new()
							input_article.title=input_title
							input_article.body=input_body
							input_article.display_title=input_display_title
							input_article.colour=input_colour
							input_title=""
							input_body=""
							input_display_title=""
							input_colour=""
							article_list.push_back(input_article)
					current_content=""
					type_stack.pop_back()
				_:
					if type_stack.back()!= "body"&&type_stack.back()!="title"&&type_stack.back()!="displayTitle":
						body_letter_reached=false
						
						if character.is_valid_identifier()||character.is_valid_integer()||character==",":
							current_content+=character
							
					else:
						if !body_letter_reached:
							if character.is_valid_identifier()||character.is_valid_integer()||character=="."||character==","||character=="?"||character=="!"||character=="\""||character=="("||character=="*"||character=="@":
								body_letter_reached=true
						if body_letter_reached:
							current_content+=character

func _on_Article_Back_pressed():
	get_parent().get_node("Back").visible=true
	get_parent().get_node("Article Back").visible=false
	$ScrollContainer.visible=true
	show_articles()
	bbcode_text=""
