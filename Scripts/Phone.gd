extends Panel

onready var money_label= get_parent().get_node("Label")

func _on_Back_pressed():
	get_parent().get_node("DialogueBox").get_node("Panel").visible=false
	for children in self.get_children():
		children.visible=false
	$"Home Screen".visible=true
	money_label.visible=true
	

func _on_1_pressed():
	print("Scene reloaded")
	get_tree().change_scene("res://Scenes/TestScene.tscn")

func _on_2_pressed():
	$"Home Screen".visible=false
	$"2 Sceen".visible=true
	money_label.visible=false


func _on_3_pressed():
	get_parent().get_node("DialogueBox").get_node("Panel").visible=true
	$"Home Screen".visible=false
	$"3 Sceen".visible=true
	money_label.visible=false


func _on_4_pressed():
	Globals.money-=10
	money_label.text=str(Globals.money)


func _on_5_pressed():
	Globals.money+=10
	money_label.text=str(Globals.money)


func _on_6_pressed():#FS
	print("Fullscreen toggled")
	OS.window_fullscreen=!OS.window_fullscreen
	
func _on_7_pressed():#save
	print("Saving game")
	Save.save_game()


func _on_8_pressed():#load
	print("Loading save")
	Save.load_globals()
	get_tree().change_scene("res://Scenes/TestScene.tscn")
	Globals.load_game=true

func _on_9_pressed():#quit
	print("Quitting to main menu")
	get_tree().change_scene("res://Scenes/MainMenu.tscn")



