extends Sprite

#variable that can be changed in the inspector, chooses what textfile to load.
export var text_file_name=""


#All of these are needed for Caravan
#Caravan is already comented completeley
#I will only be commenting on the changes in the Godot version of Caravan
#If you would like to learn more about how Caravan works please see the original c++ version.

var type_stack=[]

class Statement:
	var speaker=""
	var sprite_list=[]
	var dialogue=""
	var background=""
	var music=""
	var sfx=""
	

class Conversation:
	var title=""
	var scene=""
	var statement_list=[]

class Command:
	var setter=false
	var target=""
	var operation= ""
	var input=0

class Expression:
	var left_hand=""
	var right_hand=0
	var comparison=""

class Variable:
	var value=0
	var name=""

class Branch:
	var home_convo=""
	var destination_convo=""
	var display_name=""
	var command_list=[]
	var expression_list=[]


#inportant variables
var conversation_list=[]

var branch_list=[]

var variable_list=[]

var current_content=""

var is_comment_block=false
var dialogue_letter_reached=false
var is_first_conversation=true

var first_conversation=""

var input_title=""
var input_scene=""
var input_background=""
var input_music=""
var input_sfx=""
var input_speaker=""
var input_sprite_list=[]
var input_dialogue=""
var input_home_convo=""
var input_destination_convo=""
var input_display_name=""
var input_left_hand=""
var input_right_hand=""
var input_variable_name=""
var input_value=0
var input_comparison=""
var input_statement_list=[]
var input_expression_list=[]
var input_command_list=[]
#initialization
func _ready():
	print(Globals.current_scene)
	$Name.text=""
	$Dialogue.text=""
	if Globals.current_scene!="":
		text_file_name=Globals.current_scene
	parse_text(text_file_name)
	
	if !Globals.load_game:
		set_globals()
		target_convo=first_conversation
		find_target_convo()
		progress_dialogue()
	else:
		for children in $ScrollContainer.get_node("VBoxContainer").get_children():
			children.queue_free()
		$Name.text=""
		$Dialogue.text=""
		is_choice=false
		Save.load_game()
		for x in range(variable_list_names.size()):
			var load_var = Variable.new()
			load_var.name=variable_list_names[x]
			load_var.value=variable_list_values[x]
			variable_list.append(load_var)
		$Music.stream=load(Globals.current_music)
		$Music.play()
		progress_dialogue()
		Globals.load_game=false
		


func _input(event):
	if event is InputEventKey: 
		if event.is_action_released("Progress") && get_parent().get_node("Phone/3 Sceen").visible==false :
			progress_request()
	elif event is InputEventMouseButton:
		if event.is_action_pressed("scroll_down")&& get_parent().get_node("Phone/3 Sceen").visible==false:
			progress_request()
		if event.is_action_released("right_click")&&get_parent().get_node("Backlog").visible==true:
			is_choice=false
			get_parent().get_node("Backlog").visible=false
			
		elif (event.is_action_pressed("scroll_up")||event.is_action_released("right_click")) && get_parent().get_node("Backlog").visible==false&&get_parent().get_node("Phone/3 Sceen").visible==false:
			is_choice=true
			get_parent().get_node("Backlog/Backlog").scroll_to_line(get_parent().get_node("Backlog/Backlog").get_line_count ( )-1 )
			get_parent().get_node("Backlog").visible=true
			


#This function reads through every line in the textfile and parses it.
#This happens right at the start of the scene.
#For more information please see the original Caravan project.
func parse_text(var textfile_name):
	var filepath="res://Textfiles/SceneTextfiles/"+textfile_name+".txt"
	var textfile= File.new()
	var text_file_data=""
	#open text file to read
	if textfile.file_exists(filepath):
		textfile.open(filepath,textfile.READ)
		text_file_data= textfile.get_as_text()
		textfile.close()
		#Parses the dialogue on startup and loads the first line of dialogue
	else:
		print("textFile not found.")
		$Name.text="Error!"
		$Dialogue.text="Textfile not found!"
		is_choice=true
		return
		
	type_stack.push_back("scene");
	for character in text_file_data:
		if character =='$':
			is_comment_block=!is_comment_block
			continue
		if !is_comment_block:
			match character:
				'{':
					type_stack.push_back(current_content)
					
					current_content=""
				'}':
					match type_stack.back():
						
						"title":
							input_title=current_content
						"scene":
							input_scene=current_content
						"background":
							input_background=current_content
						"music":
							input_music=current_content
						"sfx":
							input_sfx=current_content
						"speaker":
							input_speaker=current_content
						"sprite":
							input_sprite_list.push_back(current_content)
						"dialogue":
							input_dialogue=current_content
						"homeConvo":
							input_home_convo=current_content
						"destinationConvo":
							input_destination_convo=current_content
						"displayName":
							input_display_name=current_content
						"variable":
							input_variable_name=current_content
						"value":
							input_value=current_content
						"set":
							var input_command=Command.new()
							input_command.setter=true
							input_command.target=input_variable_name
							input_command.operation="set";
							input_command.input=input_value
							input_command_list.push_back(input_command);
							input_variable_name=""
							input_value=0
						"increase":
							var input_command=Command.new()
							input_command.setter=true
							input_command.target=input_variable_name
							input_command.operation="incr";
							input_command.input=input_value
							input_command_list.push_back(input_command);
							input_variable_name=""
							input_value=0
						"decrease":
							var input_command=Command.new()
							input_command.setter=true
							input_command.target=input_variable_name
							input_command.operation="decr";
							input_command.input=input_value
							input_command_list.push_back(input_command);
							input_variable_name=""
							input_value=0
						"lefthand":
							input_left_hand=current_content
						"righthand":
							input_right_hand=current_content
						"comparison":
							match current_content:
								"lesser":
									input_comparison="lesser"
								"greater":
									input_comparison="greater"
								"eql":
									input_comparison="eql"
								"dif":
									input_comparison="dif"
						"expression":
							var input_expression=Expression.new()
							input_expression.left_hand=input_left_hand
							input_expression.right_hand=int(input_right_hand)
							input_expression.comparison=input_comparison
							input_expression_list.push_back(input_expression)
							input_left_hand=""
						"statement":
							var input_statement=Statement.new()
							input_statement.speaker=input_speaker
							input_statement.sprite_list=input_sprite_list.duplicate()
							input_statement.dialogue=input_dialogue
							input_statement.background=input_background
							input_statement.music=input_music
							input_statement.sfx=input_sfx
							input_statement_list.push_back(input_statement)
							input_speaker=""
							input_sprite_list.clear()
							input_dialogue=""
							input_background=""
							input_music=""
							input_sfx=""
						"conversation":
							var input_conversation = Conversation.new()
							input_conversation.statement_list=input_statement_list.duplicate()
							input_conversation.title=input_title
							input_conversation.scene=input_scene
							if(is_first_conversation):
								first_conversation=input_title
								is_first_conversation=false
							input_title=""
							input_scene=""
							input_statement_list.clear()
							conversation_list.push_back(input_conversation)
						"branch":
							var input_branch=Branch.new()
							input_branch.home_convo=input_home_convo
							input_branch.destination_convo=input_destination_convo
							input_branch.display_name=input_display_name
							input_branch.expression_list=input_expression_list.duplicate()
							input_branch.command_list=input_command_list.duplicate()
							input_home_convo=""
							input_destination_convo=""
							input_display_name=""
							input_expression_list.clear()
							input_command_list.clear()
							branch_list.push_back(input_branch)
					current_content=""
					type_stack.pop_back()
				_:
					if type_stack.back()!= "dialogue"&&type_stack.back()!="displayName"&&type_stack.back()!="speaker"&&type_stack.back()!="music"&&type_stack.back()!="sfx":
						dialogue_letter_reached=false
						
						if character.is_valid_identifier()||character.is_valid_integer()||character==",":
							current_content+=character
							
					else:
						if !dialogue_letter_reached:
							if character.is_valid_identifier()||character.is_valid_integer()||character=="."||character==","||character=="?"||character=="!"||character=="\""||character=="("||character=="*"||character=="@":
								dialogue_letter_reached=true
						if dialogue_letter_reached:
							current_content+=character


#To keep track of the current conversation/statement
var i=0
var j=0

onready var CaravanHandler=get_parent().get_node("Background")
 
var target_convo
var scene_running=true
var is_choice=false

#Goes through every conversation until it finds the target convo
func find_target_convo():
	j=0
	is_choice=false
	for x in range(conversation_list.size()):
		if conversation_list[x].title==target_convo:
			scene_running=true
			i=x
			break
			
		else:
			scene_running=false

#This function is called once at the start to display the first line of dialogue
#As well as everytime the user presses the button on the dialogue box.
#It progresses the dialogue one statement at a time.
var MARGIN_SPEAKER=-35
var MARGIN_NOSPEAKER= -35#-50

var backlog_speaker_color=""
func progress_dialogue():
	if scene_running:
		$Name.text=""
		$Dialogue.text=""
		if j<conversation_list[i].statement_list.size():
			if conversation_list[i].statement_list[j].speaker!="":
					$Dialogue.margin_top=MARGIN_SPEAKER
					$Name.text= conversation_list[i].statement_list[j].speaker
					if Globals.player_name.length()>10||$Name.text.length()>10:
						$nametag.visible=true
					else: 
						$nametag.visible=false
			else:
				$Dialogue.margin_top=MARGIN_NOSPEAKER
				$nametag.visible=false
			$Dialogue.visible_characters=0
			#Functions placed in the background node
			CaravanHandler.handle_speaker(conversation_list[i].statement_list[j].speaker)
			if Globals.load_game!=true:
				get_parent().get_node("Backlog/Backlog").bbcode_text+="[color="+backlog_speaker_color+"]"+conversation_list[i].statement_list[j].speaker+"[/color]"+"\n"
			CaravanHandler.handle_variables(conversation_list[i].statement_list[j].dialogue)
			CaravanHandler.handle_sprites(conversation_list[i].statement_list[j].sprite_list)
			CaravanHandler.handle_background(conversation_list[i].statement_list[j].background)
			CaravanHandler.handle_music(conversation_list[i].statement_list[j].music)
			CaravanHandler.handle_sfx(conversation_list[i].statement_list[j].sfx)
			j+=1
		else:
			is_choice=true
			handle_branches()
	#When there is no more conversations to print you've reached the end of the scene.
	else:
		Globals.cont_game=true
		Globals.global_list=variable_list
		change_scene(conversation_list[i].scene)
		print("End of textfile")
		$Dialogue.text="---THIS IS THE END---"
		$Name.text=""
		$Dialogue.get_node("Button").visible=false

var branches_to_print=[]
var last_destination=""
var last_branch_point=0

func handle_branches():
	branches_to_print=[]
	last_destination=""
	last_branch_point=0
	scene_running=false
	for a in range(branch_list.size()):
		if branch_list[a].home_convo==target_convo:
			last_branch_point=a
			var expressions_true=true
			for b in range(branch_list[a].expression_list.size()):
				if expressions_true:
					var left_hand
					var right_hand=branch_list[a].expression_list[b].right_hand
					var var_found=false
					for q in range(variable_list.size()):
						if variable_list[q].name==branch_list[a].expression_list[b].left_hand:
							var_found=true
							left_hand=variable_list[q].value
					if var_found:
						if branch_list[a].expression_list[b].comparison=="lesser":
							if left_hand>=right_hand:
								expressions_true=false
						if branch_list[a].expression_list[b].comparison=="greater":
							if left_hand<=right_hand:
								expressions_true=false
						if branch_list[a].expression_list[b].comparison=="eql":
							if left_hand!=right_hand:
								expressions_true=false
						if branch_list[a].expression_list[b].comparison=="dif":
							if left_hand==right_hand:
								expressions_true=false
					else:
						expressions_true=false
			if expressions_true:
				scene_running=true
				branches_to_print.push_back(branch_list[a])
				last_destination=branch_list[a].destination_convo
	choice_control()
#If there are any choices to print this function creates buttons for those choices
func choice_control():
	if branches_to_print.size()>1:
		for a in range(branches_to_print.size()):
			$ScrollContainer.visible=true
			$Name.text=""
			$Dialogue.text=""
			var choice_button= Button.new()
			choice_button.align=0
			choice_button.text=str(a+1)+". "+ branches_to_print[a].display_name
			#choice_button.text=" "+branches_to_print[a].display_name
			choice_button.set_name(branches_to_print[a].destination_convo)
			choice_button.connect("pressed",self,"choice_button",[a])
			$ScrollContainer.get_node("VBoxContainer").add_child(choice_button)
	else:
		$Dialogue.margin_top=MARGIN_NOSPEAKER
		target_convo=last_destination
		last_branch_point=0
		handle_vars()

#This executes when one of the created buttons is clicked
#Assigns the target convo depending on which buttons was clicked.
func choice_button(var choice):
	if scene_running:
		if Globals.load_game!=true:
			get_parent().get_node("Backlog/Backlog").bbcode_text+="[color=#e1e815]"+branches_to_print[choice].display_name+"[/color]"+"\n"+"\n"
		target_convo=branches_to_print[choice].destination_convo
		last_branch_point=choice
		$ScrollContainer.visible=false
		#Remove the buttons after one is clicked
		for children in $ScrollContainer.get_node("VBoxContainer").get_children():
			children.queue_free()
		handle_vars()

var dialogue_printed=false

#When the DialogueBOx button is clicked
func _on_Button_pressed():
	progress_request()

onready var typeSfx= get_node("Typing");

func progress_request():
	#Does not allow the player to progress the dialogue when there are choices present
	if !is_choice && progress_limit==false&&waiting_for_fade==false:
		progress_limit=true
		typeSfx.stop()
		if dialogue_printed:
			dialogue_printed=false
			progress_dialogue()
		else:
			#If the dialogue is still typing out clicking the button will autocomplete the dialogue
			#Otherwise it will progress the dialogue
			$Dialogue.set_visible_characters($Dialogue.text.length())
			dialogue_printed=true
	else:
		#When there is no more conversations to print
		print("do nothing")

func handle_vars():
	if branches_to_print.size()>0:
		for a in range(branches_to_print[last_branch_point].command_list.size()):
			var branch_at_a=branches_to_print[last_branch_point].command_list[a]
			if branch_at_a.setter:
				var var_found =false
				for b in range(variable_list.size()):
					if branch_at_a.target==variable_list[b].name:
						var_found=true
						match branch_at_a.operation:
							"set":
								variable_list[b].value=int(branch_at_a.input)
							"incr":
								variable_list[b].value+=int(branch_at_a.input)
							"decr":
								variable_list[b].value-=int(branch_at_a.input)
				if !var_found:
					var new_variable= Variable.new()
					new_variable.name=branch_at_a.target
					new_variable.value=0
					
					match branch_at_a.operation:
						"set":
							new_variable.value=int(branch_at_a.input)
						"incr":
							new_variable.value+=int(branch_at_a.input)
						"decr":
							new_variable.value-=int(branch_at_a.input)
					variable_list.push_back(new_variable)
	Globals.money=CaravanHandler.get_variable("Money")
	
	get_parent().get_node("Label").text=str(Globals.money)
	find_target_convo()
	progress_dialogue()

func set_globals():
	var global_variable=Variable.new()
	global_variable.name="Money"
	global_variable.value=Globals.money
	variable_list.push_back(global_variable)
	
	global_variable=Variable.new()
	global_variable.name="Gender"
	global_variable.value=Globals.gender
	variable_list.push_back(global_variable)
	
	if Globals.cont_game==true:
		variable_list=Globals.global_list


func change_scene(var next_scene):
	if next_scene!="":
		Globals.current_scene=next_scene
		print("Loading textfile: "+Globals.current_scene)
		get_tree().change_scene("res://Scenes/TestScene.tscn")

var variable_list_names
var variable_list_values

func save_game():
	variable_list_names=[]
	for x in range(variable_list.size()):
		variable_list_names.append(variable_list[x].name)
		
	variable_list_values=[]
	for x in range(variable_list.size()):
		variable_list_values.append(variable_list[x].value)
		
	var current_statement
	if j>0:
		current_statement=j-1
	else:
		current_statement=j
		
	var save_dict= {
		i=i,
		j=current_statement,
		target_convo=target_convo,
		variable_list_names=variable_list_names,
		variable_list_values=variable_list_values
	}
	return save_dict

func _on_Close_pressed():
	is_choice=false
	get_parent().get_node("Backlog").visible=false

var progress_limit=false

func _on_Scroll_Limit_timeout():
	progress_limit=false

var waiting_for_fade=false

func _on_FadeOut_tween_completed(object, key):
	$Music.stop()
	waiting_for_fade=false
