extends RichTextLabel

onready var typeSfx=get_parent().get_node("Typing")
#This function is called every 0.05 seconds
#This creates the typing affect
func _on_Timer_timeout():
	if get_parent().dialogue_printed==false&&get_parent().is_choice==false:
		set_visible_characters(get_visible_characters()+1)
		typeSfx.play()
	if visible_characters==text.length():
		#This bool is to check if the current dialogue line is still typing or if it's complete.
		get_parent().dialogue_printed=true
		typeSfx.stop()
	#Only activates the scrollbar on the dialogue box if the text reaches the end of it.
	if visible_characters>=180:
		scroll_active=true
	else:
		scroll_active=false
		
	if get_parent().is_choice==true:
		typeSfx.stop()


func save_game():
	var save_dict= {
		text=text,
		margin_top=margin_top
	}
	return save_dict