extends Sprite

var mask_array=["NekoMaskv1","OniMaskRabbidv1","GrreenMaskv1"]
var current_mask=0

func _ready():
	current_mask=Globals.current_mask
	$Mask.animation=mask_array[current_mask]

func _on_Down_pressed():
	if current_mask!=0:
		current_mask-=1
	else:
		current_mask=mask_array.size()-1
	$Mask.animation=mask_array[current_mask]
	Globals.current_mask=mask_array[current_mask]
	


func _on_Up_pressed():
	if current_mask!=mask_array.size()-1:
		current_mask+=1
	else:
		current_mask=0
	$Mask.animation=mask_array[current_mask]
	Globals.current_mask=current_mask
	
	
func save_game():
	var save_dict= {
		current_mask=current_mask
	}
	return save_dict
