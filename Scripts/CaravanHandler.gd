extends AnimatedSprite

#Called every time the dialogue progresses
#If there is a background tag is displays that background
#The writer can also create some transitions.
#Will be updated with more transitions
func handle_background(var background):
	var background_split=background.split(",")
	if background_split[0]!="":
		if background_split.size()==1:
			match background_split[0]:
				"flash":
					sprite_tween.interpolate_property($White,"modulate",$White.modulate,Color("ffffff"),0.5,Tween.TRANS_LINEAR,Tween.EASE_IN)
					sprite_tween.start()
					sprite_tween.interpolate_property($White,"modulate",Color("ffffff"),Color("00ffffff"),0.5,Tween.TRANS_LINEAR,Tween.EASE_IN)
					sprite_tween.start()
				"fade_out":
					fade_out_background()
					
				_:
					animation=background_split[0]
					fade_in_background()
		
	if background_split.size()==2:
		animation=background_split[0]
		match background_split[1]:
			"fade_in":
				fade_in_background()
			"flash":
				print(animation)
				sprite_tween.interpolate_property($White,"modulate",$White.modulate,Color("ffffff"),0.5,Tween.TRANS_LINEAR,Tween.EASE_IN)
				sprite_tween.start()
				sprite_tween.interpolate_property($White,"modulate",Color("ffffff"),Color("00ffffff"),0.5,Tween.TRANS_LINEAR,Tween.EASE_IN)
				sprite_tween.start()

	sprite_tween.start()

onready var name_text=get_parent().get_node("DialogueBox").get_node("Name")
onready var dialogue_text=get_parent().get_node("DialogueBox").get_node("Dialogue")


onready var sprite_tween=$Tween

onready var type_sfx=get_parent().get_node("DialogueBox").get_node("Typing")

#Called every time the dialogue progresses
#Depending on the speaker it assigns the font colour of the "Name" node.
func handle_speaker(var speaker):
	match speaker:
		"Player":
			if Globals.player_name!="":
				name_text.text=Globals.player_name
			else:
				name_text.text="Player"
			name_text.add_color_override("font_color",Color("#00e6e6"))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#00e6e6"
		"Sammy":
			name_text.add_color_override("font_color",Color("#33cc33"))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#33cc33"
		"Shade":
			name_text.add_color_override("font_color",Color("#ffff00"))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#ffff00"
		"Dak9":
			name_text.add_color_override("font_color",Color("#cc0099"))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#cc0099"
		"Phantom":
			name_text.add_color_override("font_color",Color("#AA5588"))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#AA5588"
		_:
			#type_sfx.stream=load("res://Sound/Speech1.wav")
			name_text.add_color_override("font_color",Color(Color("#ffffff")))
			get_parent().get_node("DialogueBox").backlog_speaker_color="#ffffff"
			

var last_num_of_sprites=0

#Constants for the sprite positions.
var MID=Vector2(180,65)
var LEFT=Vector2(-160,65)
var RIGHT=Vector2(180,65)
var MID_LEFT=Vector2(-80,10)
var MID_RIGHT=Vector2(80,10)

#Variables that can be changed in the inspector, for speed of the sprite animations
export var move_speed=1.0
export var fade_speed=1.0
export var bg_fade_speed=1.0




#Called every time the dialogue progresses
#It checks how many sprites were active and how many are now active
#Then plays an animation accordingly.
func handle_sprites(var sprite):
	if last_num_of_sprites!=sprite.size():
		match last_num_of_sprites:
			0:
				match sprite.size():
					1:
						$Sprite1.animation=sprite[0]
						$Sprite1.position=RIGHT
						fade_in($Sprite1)
					2:
						$Sprite1.animation=sprite[0]
						$Sprite1.position=RIGHT
						fade_in($Sprite1)
						$Sprite2.animation=sprite[1]
						$Sprite2.position=LEFT
						fade_in($Sprite2)
			1:
				match sprite.size():
					0:
						if $Sprite1.modulate ==Color("ffffff") :
							fade_out($Sprite1)
						if $Sprite2.modulate ==Color("ffffff") :
							fade_out($Sprite2)

					2:
						$Sprite1.animation=sprite[0]
						$Sprite1.position=RIGHT
						$Sprite1.modulate=Color("ffffff")
						$Sprite2.animation=sprite[1]
						$Sprite2.position=LEFT
						fade_in($Sprite2)
			2:
				match sprite.size():
					0:
						if $Sprite1.modulate ==Color("ffffff") :
							fade_out($Sprite1)
						if $Sprite2.modulate ==Color("ffffff") :
							fade_out($Sprite2)

					1:
						if sprite[0]==$Sprite1.animation:
							fade_out($Sprite2)
						else:
							$Sprite2.animation=sprite[0]
							sprite_tween.interpolate_property($Sprite2,"position",LEFT,RIGHT,move_speed,Tween.TRANS_LINEAR,Tween.EASE_IN)
							sprite_tween.start()
							fade_out($Sprite1)

	else:
		match sprite.size():
			1:
				$Sprite1.animation=sprite[0]
			2:
				$Sprite1.animation=sprite[0]
				$Sprite2.animation=sprite[1]
			3:
				$Sprite1.animation=sprite[0]
				$Sprite2.animation=sprite[1]
				$Sprite3.animation=sprite[2]
	last_num_of_sprites=sprite.size()

func fade_in(sprite_to_fade,fade_speeds=fade_speed):
	get_parent().get_node("DialogueBox").is_choice=true
	var t = Timer.new()
	t.set_wait_time(fade_speeds/4)
	t.set_one_shot(true)
	self.add_child(t)
	sprite_to_fade.modulate=Color("000000")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("3f3f3f")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("7f7f7f")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("ffffff")
	t.queue_free()
	get_parent().get_node("DialogueBox").is_choice=false
	
func fade_out(sprite_to_fade):
	get_parent().get_node("DialogueBox").is_choice=true
	var t = Timer.new()
	t.set_wait_time(fade_speed/4)
	t.set_one_shot(true)
	self.add_child(t)
	sprite_to_fade.modulate=Color("7f7f7f")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("3f3f3f")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("000000")
	t.start()
	yield(t, "timeout")
	sprite_to_fade.modulate=Color("00000000")
	t.queue_free()
	get_parent().get_node("DialogueBox").is_choice=false

func fade_out_background():
	get_parent().get_node("DialogueBox").is_choice=true
	var t = Timer.new()
	t.set_wait_time(bg_fade_speed/4)
	t.set_one_shot(true)
	self.add_child(t)
	self_modulate=Color("bebebe")
	t.start()
	yield(t, "timeout")
	self_modulate=Color("7f7f7f")
	t.start()
	yield(t, "timeout")
	self_modulate=Color("3f3f3f")
	t.start()
	yield(t, "timeout")
	self_modulate=Color("000000")
	t.queue_free()
	get_parent().get_node("DialogueBox").is_choice=false
	
	
func fade_in_background():
	get_parent().get_node("DialogueBox").is_choice=true
	var t = Timer.new()
	t.set_wait_time(bg_fade_speed/4)
	t.set_one_shot(true)
	self.add_child(t)
	self_modulate=Color("ffffff")
	modulate=Color("3f3f3f")
	t.start()
	yield(t, "timeout")
	modulate=Color("7f7f7f")
	t.start()
	yield(t, "timeout")
	modulate=Color("bebebe")
	t.start()
	yield(t, "timeout")
	modulate=Color("ffffff")
	t.queue_free()
	get_parent().get_node("DialogueBox").is_choice=false
	
	

var replacement_needed=false
var variable_to_be_replaced= ""
var new_dialogue=""
func handle_variables(var dialogue):
	new_dialogue=""
	for character in dialogue:
		if character=='@':
			replacement_needed=true
			continue
		if replacement_needed:
			if character!= '-' && character!=" ":
				variable_to_be_replaced+=character
			else:
				replace_variable()
				variable_to_be_replaced=""
				replacement_needed=false
				if character == " ":
					new_dialogue+= " "
					continue
		else:
			new_dialogue+= character 
	dialogue_text.text=new_dialogue
	if Globals.load_game!=true:
		get_parent().get_node("Backlog/Backlog").bbcode_text+=new_dialogue+"\n"+"\n"

func replace_variable():
	match variable_to_be_replaced:
		"Player":
			if Globals.player_name!="":
				new_dialogue+=Globals.player_name
			else:
				new_dialogue+= "Player"
		"Money":
			new_dialogue+=str(Globals.money)
		#Special case for pronouns
		1:
			match Globals.gender:
				0:
					new_dialogue+="Male"
				1:
					new_dialogue+="Female"
		"She":
			match Globals.gender:
				0:
					new_dialogue+="He"
				1:
					new_dialogue+="She"
		"HerSubject":
			match Globals.gender:
				0:
					new_dialogue+="Him"
				1:
					new_dialogue+="Her"
		"HerObject":
			match Globals.gender:
				0:
					new_dialogue+="His"
				1:
					new_dialogue+="Her"
		"Hers":
			match Globals.gender:
				0:
					new_dialogue+="His"
				1:
					new_dialogue+="Hers"
		"Woman":
			match Globals.gender:
				0:
					new_dialogue+="Man"
				1:
					new_dialogue+="Woman"
		#Lower case
		1:
			match Globals.gender:
				0:
					new_dialogue+="male"
				1:
					new_dialogue+="female"
		"she":
			match Globals.gender:
				0:
					new_dialogue+="he"
				1:
					new_dialogue+="she"
		"herSubject":
			match Globals.gender:
				0:
					new_dialogue+="him"
				1:
					new_dialogue+="her"
		"herObject":
			match Globals.gender:
				0:
					new_dialogue+="his"
				1:
					new_dialogue+="her"
		"hers":
			match Globals.gender:
				0:
					new_dialogue+="his"
				1:
					new_dialogue+="hers"
		"woman":
			match Globals.gender:
				0:
					new_dialogue+="man"
				1:
					new_dialogue+="woman"
		
		_:
			new_dialogue+=str(get_variable(variable_to_be_replaced))

func get_variable(var string_variable):
	var variable_list=get_parent().get_node("DialogueBox").variable_list
	for i in range(variable_list.size()):
		if variable_list[i].name==string_variable:
			return variable_list[i].value
	return null

onready var music_player=get_parent().get_node("DialogueBox").get_node("Music")
onready var music_tween=get_parent().get_node("DialogueBox").get_node("Music").get_node("FadeOut")

func music_fade_out(music_player):
    # tween music volume down to 0
	music_tween.interpolate_property(music_player, "volume_db", music_player.volume_db, -80, 1, 1, Tween.EASE_IN, 0)
	music_tween.start()
    # when the tween ends, the music will be stopped


func handle_music(music):
	if music!="":
		if music=="fade_out":
			get_parent().get_node("DialogueBox").waiting_for_fade=true
			Globals.current_music=""
			music_fade_out(music_player)
		else:
			music_player.stream=load("res://Sound/"+music)
			Globals.current_music="res://Sound/"+music
			music_player.volume_db=-5
			music_player.play()

onready var sfx_player=get_parent().get_node("DialogueBox").get_node("Sfx")

func handle_sfx(sfx):
	if sfx!="":
		sfx_player.stream=load("res://Sound/"+sfx)
		sfx_player.play()

func save_game():
	var save_dict={
		animation=self.animation,
		self_modulate={
			m1=self_modulate[0],
			m2=self_modulate[1],
			m3=self_modulate[2],
			m4=self_modulate[3]
		}
	}
	return save_dict