extends AnimatedSprite

func save_game():
	var save_dict={
		animation=self.animation,
		pos={
			x=position.x,
			y=position.y
		},
		modulate={
			m1=modulate[0],
			m2=modulate[1],
			m3=modulate[2],
			m4=modulate[3]
		}
	}
	return save_dict
