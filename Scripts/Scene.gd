extends Node

func _ready():
	Globals.current_scene=$DialogueBox.text_file_name

func save_game():
	var save_dict={
		money=Globals.money,
		gender=Globals.gender,
		player_name=Globals.player_name,
		current_scene=Globals.current_scene,
		current_mask=Globals.current_mask,
		current_music=Globals.current_music
	}
	return save_dict
