extends Node

const SAVE_PATH="user://TPDSAVEGAME.json"
var _settings={}

func _ready():
	pass

func save_game():
	#Get all of the save data from persistent nodes
	var save_dict={}
	var nodes_to_save=get_tree().get_nodes_in_group("Persistent")
	for node in nodes_to_save:
		save_dict[node.get_path()] =node.save_game()
	
	#Create a save file
	var save_file=File.new()
	save_file.open(SAVE_PATH,File.WRITE)
	
	#Serialize the data dictionairy to JSON
	save_file.store_line(to_json(save_dict))
	
	#Write the JSON to the file and save to disk
	save_file.close()


func load_game():
	#Try to load a saved file
	var save_file =File.new()
	if not save_file.file_exists(SAVE_PATH):
		return
		
	#Parse the file data if it exists
	save_file.open(SAVE_PATH,File.READ)
	var data=parse_json(save_file.get_as_text())
	#Load the data into persistent nodes
	for node_path in data.keys():
		var node=get_node(node_path)
		for attribute in data[node_path]:
			match attribute:
				
				"pos":
					node.position=(Vector2(data[node_path]['pos']['x'],data[node_path]['pos']['y']))
				"modulate":
					node.modulate=(Color(data[node_path]['modulate']['m1'],data[node_path]['modulate']['m2'],data[node_path]['modulate']['m3'],data[node_path]['modulate']['m4']))
				"self_modulate":
					node.modulate=(Color(data[node_path]['self_modulate']['m1'],data[node_path]['self_modulate']['m2'],data[node_path]['self_modulate']['m3'],data[node_path]['self_modulate']['m4']))
				_:
					node.set(attribute,data[node_path][attribute])


func load_globals():
	#Try to load a saved file
	var save_file =File.new()
	
	if not save_file.file_exists(SAVE_PATH):
		return
		
	#Parse the file data if it exists
	save_file.open(SAVE_PATH,File.READ)
	var data=parse_json(save_file.get_as_text())
	
	for node_path in data.keys():
		
		for attribute in data[node_path]:
				match attribute:
					"money":
						Globals.money=data[node_path]['money']
					"gender":
						Globals.gender=data[node_path]['gender']
					"player_name":
						Globals.player_name=data[node_path]['player_name']
					"current_scene":
						Globals.current_scene=data[node_path]['current_scene']
					"current_mask":
						Globals.current_mask=data[node_path]['current_mask']
					"current_music":
						Globals.current_music=data[node_path]['current_music']
			