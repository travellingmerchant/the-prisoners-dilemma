extends Node2D

var maskMenu
var maskMenuShown = 0
var mask

func _ready():
	maskMenu = get_node("MaskMenu")
	mask = get_node("Mask")

#func _process(delta):
#	pass

func _on_ChangeMaskButton_pressed():
	print("reee")
	if maskMenuShown == 1:
		maskMenu.hide()
		maskMenuShown = 0
	else:
		_menuSetup()

func _menuSetup():
	maskMenu.clear()
	maskMenu.add_item("Blue", 0)
	maskMenu.add_item("Red", 1)
	maskMenu.add_item("Purple", 2)
	maskMenu.add_item("Green", 3)
	maskMenu.add_item("Yellow", 4)
	maskMenu.set_position(Vector2(20,120))
	maskMenu.connect("id_pressed", self, "_popupMenuChoice")
	maskMenu.show()
	maskMenuShown = 1

func _popupMenuChoice(ID):
	match(ID):
		0: mask.animation = "Blue"
		1: mask.animation = "Red"
		2: mask.animation = "Purple"
		3: mask.animation = "Green"
		4: mask.animation = "Yellow"
	maskMenuShown = 0
