extends Node

#Allows us to select which scene to load from the inspector
export var start_scene=""

func _ready():
	$PixelPerfect.pressed=PixelPerfectScaling.pixel_perfect

#When the button is pressed it will load whatever scene we entered in the inspector
func _on_Start_Button_pressed():
	Globals.player_name=$LineEdit.text
	Globals.money=0
	Globals.load_game=false
	Globals.current_scene=""
	Globals.current_mask=0
	Globals.global_list=[]
	Globals.cont_game=false
	
	
	if $Male.pressed==true:
		Globals.gender=0
	else:
		Globals.gender=1
	if $LineEdit2.text!="":
		Globals.current_scene=$LineEdit2.text;
		print("Loading textfile: "+Globals.current_scene)
	get_tree().change_scene("res://Scenes/TestScene.tscn")


func _on_CheckBox_pressed():
	OS.window_fullscreen=!OS.window_fullscreen


func _on_Male_pressed():
	$Male.pressed=true
	$Female.pressed=false


func _on_Female_pressed():
	$Female.pressed=true
	$Male.pressed=false


func _on_Load_Button_pressed():
	Save.load_globals()
	get_tree().change_scene("res://Scenes/TestScene.tscn")
	Globals.load_game=true
	


func _on_PixelPerfect_pressed():
	PixelPerfectScaling.pixel_perfect=!PixelPerfectScaling.pixel_perfect
	$PixelPerfect.toggle_mode
	OS.window_fullscreen=false