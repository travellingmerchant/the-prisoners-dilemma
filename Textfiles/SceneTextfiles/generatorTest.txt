conversation
{
    title{Home}
    
    statement
    {
        speaker{Player}
        audio{see}
        background{sammy_bar}
        dialogue{Sammy's bar...}
    }

    
}

conversation
{
    title{WhatDoHub}
    statement
    {
        dialogue{What do you want to do?}
        audio{PASSEPIED - Nagasugita Haru.ogg}
    }
}

conversation
{
    title{Speak to Sammy} 
}

branch
{
    homeConvo{WhatDoHub}
    destinationConvo{SpeakToSammyGreet}
    displayName{Speak to Sammy}
}

branch
{
    homeConvo{WhatDoHub}
    destinationConvo{Leave}
    displayName{Leave}
}

branch
{
    homeConvo{Home}
    destinationConvo{WhatDoHub}
    
    set{variable{HowIsSammy} value{1}}
}

conversation
{
    title{TalkToSammy-HowAre1}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{I’m doing just fine.}
    }
    
}

conversation
{
    title{TalkToSammy-CanCoffee1}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Sure, if you have 20 credits on you.}
    }
    statement
    {
        speaker{Player}
        sprite{Sammy_Idle}
        dialogue{...}
    }
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Get your broke ass out of here.}
    }
    
}

conversation
{
    title{TalkToSammy-BeSeeing1}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Ciao.}
    }
    
}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-HowAre1}
    displayName{How are you Sammy?}

    expression
    {
        lefthand{HowIsSammy}
        righthand{1}
        comparison{eql}
    } 
}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-HowAre2}
    displayName{How are you Sammy?}
    
    expression
    {
        lefthand{HowIsSammy}
        righthand{2}
        comparison{eql}
    }   

}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-HowAre3}
    displayName{How are you Sammy?}
    
    expression
    {
        lefthand{HowIsSammy}
        righthand{3}
        comparison{eql}
    }   

}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-HowAre4}
    displayName{How are you Sammy?}
    
    expression
    {
        lefthand{HowIsSammy}
        righthand{3}
        comparison{greater}
    }   

}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-CanCoffee1}
    displayName{Can I have some coffee Sammy?}
}

branch
{
    homeConvo{Speak to Sammy}
    destinationConvo{TalkToSammy-BeSeeing1}
    displayName{Be seeing you Sammy.}
}


branch
{
    homeConvo{TalkToSammy-HowAre1}
    destinationConvo{Speak to Sammy}

    increase{variable{HowIsSammy} value{1}}
}


branch
{
    homeConvo{TalkToSammy-CanCoffee1}
    destinationConvo{Speak to Sammy}
    
}

branch
{
    homeConvo{TalkToSammy-BeSeeing1}
    destinationConvo{WhatDoHub}
    
}

conversation
{
    title{SpeakToSammyGreet}
    
    statement
    {
        speaker{Player}
        sprite{Sammy_Idle}
        dialogue{Sammy}
    }
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{...}
    }
    
}

branch
{
    homeConvo{SpeakToSammyGreet}
    destinationConvo{Speak to Sammy}
    
}

conversation
{
    title{TalkToSammy-HowAre2}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Uhh, I just said I was doing fine...}
    }
    
}

conversation
{
    title{TalkToSammy-HowAre3}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Are you messing with me?}
    }
    
}

conversation
{
    title{TalkToSammy-HowAre4}
    
    statement
    {
        speaker{Sammy}
        sprite{Sammy_Idle}
        dialogue{Buzz off.}
    }
    
}



branch
{
    homeConvo{TalkToSammy-HowAre2}
    destinationConvo{Speak to Sammy}
    
    
    increase{variable{HowIsSammy} value{1}}
    
}

branch
{
    homeConvo{TalkToSammy-HowAre3}
    destinationConvo{Speak to Sammy}
    
    
    increase{variable{HowIsSammy} value{1}}
    
}

branch
{
    homeConvo{TalkToSammy-HowAre4}
    destinationConvo{Speak to Sammy}
    
    
    increase{variable{HowIsSammy} value{1}}
    
}