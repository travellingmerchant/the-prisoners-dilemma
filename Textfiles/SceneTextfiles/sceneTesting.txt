conversation
{
    title{Start}
    statement{
    dialogue{This is the beginning}}
}

branch
{
    homeConvo{Start}
    destinationConvo{Money}
}

conversation
{
    title{Money}
    statement{
    dialogue{What would you like to do?}}
}

branch
{
    homeConvo{Money}
    destinationConvo{Money}
    displayName{Increase}
    increase{variable{money}value{100}}
}

branch
{
    homeConvo{Money}
    destinationConvo{Money}
    displayName{Decrease}
    decrease{variable{money}value{100}}
}

branch
{
    homeConvo{Money}
    destinationConvo{Quit}
    displayName{Quit}
}

conversation
{
    title{Quit}
    statement{
    dialogue{Quit}}
    scene{MainMenu}
}